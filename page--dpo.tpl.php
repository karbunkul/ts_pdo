<!-- Main container -->
<div class="page-container">

<!-- Hero Bloc -->
<div id="hero-bloc" class="bloc hero bg-tn-204126-125172c4fd87 bgc-maroon-htmlcss d-bloc">
	<div class="hero-nav">
		<div class="container">
			<nav class="navbar row">
				<div class="navbar-header">
						<a class="navbar-brand" href="/"><img class="small" src="<?php echo drupal_get_path('module','ts_dpo') . '/img/logo-small.png';?>" height="60"/><img class="big" src="<?php echo drupal_get_path('module','ts_dpo') . '/img/logo-big.png';?>" height="60"/></a>
						<button id="nav-toggle" type="button" class="ui-navbar-toggle navbar-toggle" data-toggle="collapse" data-target=".navbar-1">
							<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
						</button>
					</div>
		
				<div class="collapse navbar-collapse navbar-1">
					<ul class="site-navigation nav navbar-nav pull-right">
						<li>
							<strong><a href="tel:+73452422447">+7 (3452) 422747</a></strong>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
	<div class="container">
		<div class="header col-lg-offset-2 col-md-offset-2">Ваше дополнительное образование</div>
	
		
		<ul class="krugi">
			<li><div class="krug fa fa-file-text-o"></div><div>Диплом  о<br/> переподготовке</div></li>
			<li><div class="krug fa fa-calendar"></div><div>Учеба от<br/> 5 месяцев</div></li>
			<li><div class="krug fa fa-child"></div><div>Без отрыва от<br/> учебы и работы</div></li>
		</ul>
		
	</div>
	
	<div class="v-center text-center">
		<div class="vc-content">
			<a href="#order" class="btn btn-xl btn-medium-sea-green"><span class="fa fa-edit icon-spacer"></span>Оставить заявку</a><a id="scroll-hero" class="btn-dwn" href="#order"><span class="fa fa-chevron-down"></span></a>
		</div>
	</div>
</div>
<!-- Hero Bloc END -->

<!-- bloc-2 -->
<div class="bloc bgc-twilight-lavender d-bloc" id="bloc-2">
	<div class="container bloc-md">
		<div class="row">
			<div class="col-sm-4">
				<div class="text-center">
					<span class="fa fa-mortar-board icon-global icon-round icon-md icon-white"></span>
				</div>
				<h3 class="text-center mg-md">
					Широкий выбор
				</h3>
				<p class="text-center">
					программ профессиональной переподготовки и курсов повышения квалификации
				</p>
			</div>
			<div class="col-sm-4">
				<div class="text-center">
					<span class="fa fa-rocket icon-round icon-md icon-white"></span>
				</div>
				<h3 class="text-center mg-md">
					Направленность
				</h3>
				<p class="text-center">
					с которыми сталкиваются специалисты и руководители строительной отрасли
				</p>
			</div>
			<div class="col-sm-4">
				<div class="text-center">
					<span class="fa fa-cubes icon-puzzle icon-bike icon-round icon-md icon-white"></span>
				</div>
				<h3 class="text-center mg-md">
					Новые программы
				</h3>
				<p class="text-center">
					мы разрабатываем и реализовываем программы по Вашему заказу!
				</p>
			</div>
		</div>
	</div>
</div>
<!-- bloc-2 END -->

<!-- bloc-3 -->
<div class="bloc bgc-twilight-lavender d-bloc" id="bloc-3">
	<div class="container bloc-md">
		<div class="row">
			<div class="col-sm-6">
				<div class="text-center">
					<span class="fa fa-money icon-round icon-md icon-white"></span>
				</div>
				<h3 class="text-center mg-md">
					Выгодные для Вас условия обучения
				</h3>
				<p class="text-center">
					гибкий график обучения: занятия без отрыва от учебы или работы
				</p>
			</div>
			<div class="col-sm-6">
				<div class="text-center">
					<span class="fa fa-institution icon-round icon-md icon-white"></span>
				</div>
				<h3 class="text-center mg-md">
					Современные условия для обучения
				</h3>
				<p class="text-center">
					наши преподаватели владеют богатыми теоретическими и практическими знаниями
				</p>
			</div>
		</div>
	</div>
</div>
<!-- bloc-3 END -->

<!-- ScrollToTop Button -->
<a class="bloc-button btn btn-d scrollToTop"><span class="fa fa-chevron-up"></span></a>
<!-- ScrollToTop Button END-->


<!-- Footer - bloc-4 -->
<div class="bloc bgc-pale-gold l-bloc" id="bloc-4">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="mg-md tc-outer-space text-center">
					Остались вопросы? Звоните 8 (3452) 422 - 747 или оставьте заявку
				</h3>
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-4 END -->

<!-- Footer - bloc-5 -->
<div class="bloc l-bloc bgc-white" id="bloc-5">
	<div class="container bloc-md">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="mg-md">
					<span class="fa fa-bullhorn"></span> УСПЕЙ ПОДАТЬ ЗАЯВКУ
				</h1>
				<p>
					Оставьте заявку, и мы свяжемся с Вами удобным для Вас способом
				</p>
				<div class="row">
					                    <?php print render($ts_dpo_form); ?>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-5 END -->

<!-- Footer - bloc-6 -->
<div class="bloc bgc-pale-gold l-bloc" id="bloc-6">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="mg-md tc-outer-space text-center">
					Наши программы
				</h3>
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-6 END -->

<!-- Bloc Group -->
<div class='bloc-group'>

<!-- Footer - bloc-7 -->
<div class="bloc l-bloc bgc-white bloc-tile-2" id="bloc-7">
	<div class="container bloc-md">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel">
					<div class="panel-heading">
						<h3 class="mg-clear">
							Переподготовка
						</h3>
					</div>
					<div class="panel-body">
						<p>
							Мы занимаемся переподготовкой профессиональных кадров - это программы средней продолжительности, направленные на комплексное углубление знаний специалиста сметчика, экономиста, специалиста в области права и других профессий. Именно такие программы дают возможность кардинально изменить сферу Вашей деятельности
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-7 END -->

<!-- Footer - bloc-8 -->
<div class="bloc l-bloc bgc-white bloc-tile-2" id="bloc-8">
	<div class="container bloc-md">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel">
					<div class="panel-heading">
						<h3 class="mg-clear">
							Повышение квалификации
						</h3>
					</div>
					<div class="panel-body">
						<p>
							Данные программы разработаны для специалистов, по роду своей деятельности, желающие углубить и расширить свои знания и навыки в выбранной области. Курсы проводятся по разным направлениям и рассматривают отдельному тему в каждой области более подробно и детально. В конце обучения Вы получите удостоверение о повышении квалификации установленного образца
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-8 END -->
</div>
<!-- Bloc Group END -->

<!-- Footer - bloc-9 -->
<div class="bloc l-bloc bgc-pale-gold" id="bloc-9">
	<div class="container bloc-md">
		<div class="row">
			<div class="text-center">
				<a href="#order" class="btn  btn-d  btn-lg"><span class="fa fa-edit icon-spacer"></span>Оставить заявку</a>
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-9 END -->
<div id="block-photo" class="bloc l-bloc bgc-white">
	<div class="container">
	
		<div class="row" style="text-align: center;">
			<div class="col-sm-4 center">
			<h2 class="mg-md" style="text-transform: uppercase; text-align: center; margin-bottom: 41px;">
					 Лицензия
			</h2>
				<a class="fancybox fancybox.iframe" data-fancybox-type="iframe" rel="group1" href="<?php echo url(drupal_get_path('module','ts_dpo'). '/img/license.jpg',array('absolute' => true))?>"><img class="img-thumbnail" src="<?php echo url(drupal_get_path('module','ts_dpo')."/img/license-thumbnail.jpg")?>" /></a>
			</div>
			
			<div class="col-sm-4">
			<h2 class="mg-md" style="text-transform: uppercase; text-align: center; margin-bottom: 60px;">
					 Диплом о переподготовке
			</h2>
				<a class="fancybox fancybox.iframe" data-fancybox-type="iframe" rel="group1" href="<?php echo url(drupal_get_path('module','ts_dpo'). '/img/diplom3.jpg',array('absolute' => true))?>"><img style="max-width: 170px;" class="img-thumbnail" src="<?php echo url(drupal_get_path('module','ts_dpo')."/img/diplom3-thumbnail.jpg")?>" /></a>
				<a class="fancybox fancybox.iframe" data-fancybox-type="iframe" rel="group1" href="<?php echo url(drupal_get_path('module','ts_dpo')."/img/diplom11.jpg",array('absolute' => true))?>" ><img style="max-width: 170px;" class="img-thumbnail" src="<?php echo drupal_get_path('module','ts_dpo')?>/img/diplom1-thumbnail.jpg" /></a>
			</div>
			
			<div class="col-sm-4 center">
				<h2 class="mg-md" style="text-transform: uppercase; text-align: center;">
					Удостоверение о повышении квалификации
			</h2>
				<a class="fancybox fancybox.iframe" data-fancybox-type="iframe" rel="group1" href="<?php echo url(drupal_get_path('module','ts_dpo')."/img/udostover.jpg",array('absolute' => true))?>"><img class="img-thumbnail" src="<?php echo drupal_get_path('module','ts_dpo')?>/img/udostover-thumbnail.jpg" /></a>
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-10 -->
<div class="bloc1 l-bloc1 bgc-white" id="bloc-10" style="margin-bottom:0;">
	<div class="container-fluid1">
		<div class="row">
			<div class="col-sm-12">
				<a class="dg-widget-link" href="http://2gis.ru/tyumen/profiles/1830115629597758,1830115629621893/center/65.51322549999998,57.16740644566291/zoom/18?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Тюмени</a><script charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":"100%","height":610,"borderColor":"#a3a3a3","pos":{"lat":57.16740644566291,"lon":65.51322549999998,"zoom":18},"opt":{"city":"tyumen"},"org":[{"id":"1830115629597758"},{"id":"1830115629621893"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
			</div>
		</div>
	</div>
</div>


<!-- Footer - bloc-10 END -->

<!-- Footer - bloc-10 -->
<div class="bloc bgc-outer-space d-bloc" id="bloc-10">
	<div class="container bloc-md">
		<div class="row">
			<div class="col-sm-10 col-md-6 col-md-offset-2">
				<div class="col-sm-3 col-xs-3 text-center">
					<a class="social-md" href="https://www.linkedin.com/profile/view?id=348122732&trk=nav_responsive_tab_profile" target="_blank"><span class="fa fa-linkedin icon-md"></span></a>
				</div>
				<div class="col-sm-3 col-xs-3 text-center">
					<a class="social-md" href="https://www.facebook.com/dpo.institut" target="_blank"><span class="fa fa-facebook icon-md"></span></a>
				</div>
				<div class="col-sm-3 col-xs-3 text-center">
					<a class="social-md" href="https://vk.com/idpo_tgasu" target="_blank"><span class="fa fa-vk icon-md"></span></a>
				</div>
				<div class="col-sm-3 col-xs-3 text-center">
					<a class="social-md" href="http://instagram.com/dpo_tgasu"  target="_blank"><span class="fa fa-instagram icon-md"></span></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-10 END -->
</div>
<!-- Main container END -->
